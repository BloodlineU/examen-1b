package com.example.math

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Main_game.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Main_game.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Main_game : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null


    internal lateinit var operation_title: TextView
    internal lateinit var round_text: TextView
    internal lateinit var score_text: TextView
    internal lateinit var time_left_text: TextView
    internal lateinit var first_number: TextView
    internal lateinit var second_number: TextView
    internal lateinit var symbol: TextView
    internal lateinit var send_button: Button
    internal lateinit var answer_input: TextView

    var score = 0
    var time_left = 10
    var round = 1
    var currentSymbol = "none"

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer


    val args: Main_gameArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {



        operation_title = view.findViewById(R.id.operation_title)
        round_text = view.findViewById(R.id.round_text)
        score_text = view.findViewById(R.id.score_text)
        time_left_text = view.findViewById(R.id.time_left_text)
        first_number = view.findViewById(R.id.first_number)
        second_number = view.findViewById(R.id.second_number)
        symbol = view.findViewById(R.id.symbol)
        send_button = view.findViewById(R.id.send_button)
        answer_input = view.findViewById(R.id.answer_input)

        send_button.setOnClickListener{checkResult()}

        when (args.operationType) {
            "Addition" -> currentSymbol= "+"
            "Subtraction" -> currentSymbol = "-"
            "Multiplication" -> currentSymbol = "x"
            "Division" -> currentSymbol = "÷"
        }
        symbol.text = currentSymbol
        operation_title.text = args.operationType


        first_number.text = (1..20).shuffled().first().toString()
        second_number.text = (1..20).shuffled().first().toString()
        score_text.text = getString(R.string.score_text,score)
        round_text.text = getString(R.string.round_text,round)
        time_left_text.text = getString(R.string.time_left_text,time_left)

        countDownTimer = object : CountDownTimer(initialCountDown,countDownInterval) {
            override fun onFinish() {
                Toast.makeText(activity, "Its over", Toast.LENGTH_LONG).show()
                resetGame()
            }
            override fun onTick(millisUntilFinished: Long) {
                time_left = millisUntilFinished.toInt() / 1000
                time_left_text.text = getString(R.string.time_left_text,time_left)
            }
        }

        countDownTimer.start()
    }

    private fun checkResult(){
        var result = 0.0

        when (currentSymbol) {
            "+" -> result = first_number.text.toString().toDouble() + second_number.text.toString().toDouble()
            "-" -> result = first_number.text.toString().toDouble() - second_number.text.toString().toDouble()
            "x" -> result = first_number.text.toString().toDouble() * second_number.text.toString().toDouble()
            "÷" -> result = first_number.text.toString().toDouble() / second_number.text.toString().toDouble()
        }
        if(answer_input.text.toString().toDouble() == result){
            incrementScore()
        }
    }

    private fun incrementScore () {
        when (time_left){
            0 -> score += 10
            1 -> score += 10
            2 -> score += 10
            3 -> score += 10
            4 -> score += 10
            5 -> score += 50
            6-> score += 50
            7-> score += 50
            8 -> score +=100
            9 -> score +=100
            10 -> score +=100
        }

        first_number.text = (1..20).shuffled().first().toString()
        second_number.text = (1..20).shuffled().first().toString()
        score_text.text = getString(R.string.score_text,score)
        round_text.text = getString(R.string.round_text,round)
        round ++
        countDownTimer.start()

        score_text.text = getString(R.string.score_text,score)
        round_text.text = getString(R.string.round_text,round)
        time_left_text.text = getString(R.string.time_left_text,time_left)
        resetGame()

    }

    private fun resetGame() {

        score_text.text = getString(R.string.score_text,score)
        time_left_text.text = getString(R.string.time_left_text,time_left)



    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Main_game.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Main_game().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
